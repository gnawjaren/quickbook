local f
local item
local g_item
local itemLink

f = CreateFrame("Frame",nil,UIParent)
f:SetFrameStrata("HIGH")
f:RegisterEvent("ADDON_LOADED");
f:RegisterEvent("PLAYER_LOGOUT");
f:SetMovable(true)
f:EnableMouse(true)
f:RegisterForDrag("LeftButton")
f:SetScript("OnDragStart", f.StartMoving)
f:SetScript("OnDragStop", f.StopMovingOrSizing)
f:Hide()


function f:OnEvent(event, arg1)
    if event == "ADDON_LOADED" and arg1 == "quickbook" then
         if qb_groups == nil then
            qb_groups = {}
        end
        if qb_global_tabs == nil then
            qb_global_tabs = {}
        end
        if qb_global_groups == nil then
            qb_global_groups = {}
        end
        if qb_global_tabs == nil then
            qb_global_tabs = {}
        else
            for i = 0, #qb_global_tabs, 1
            do
                if qb_global_groups[qb_global_tabs[i]] then
                    qb_groups[qb_global_tabs[i]] = qb_global_groups[qb_global_tabs[i]]
                end
            end
        end
        if qb_numTabs == nil then
            qb_numTabs = 4
        end
        if qb_itemSize == nil then
            qb_itemSize = 32
        end
        if qb_rowCount == nil then
            qb_rowCount = 4
        end
        if qb_columnCount == nil then
            qb_columnCount = 4
        end
        if qb_fontSize == nil then
            qb_fontSize = 12
        end
        if qb_fontSizeSM == nil then
            qb_fontSizeSM = 10
        end
        if qb_currentGroup == nil then
            qb_currentGroup = 0
        end
        --createFrame()
    end
end

f:SetScript("OnEvent", f.OnEvent);
--f.frames = {}

local function getGroupDescription(number)
    local group = qb_groups[number]
    if group == nil then
        return number
    end
    return group.description
end

local function setGroupDescription(number, description)
    local group = qb_groups[number]
    if group == nil then
        group = {rows = {}}
    end
    group.description = description
    qb_groups[number] = group
end

local function updateHeader(f)
    if f.tabs == nil then
        f.header = CreateFrame("Frame", nil, f)
        f.header:SetWidth(qb_itemSize*qb_columnCount)
        f.header:SetHeight(16)
        f.header:SetPoint("TOPLEFT", 0, 0)
        f.header.text = f.header:CreateFontString(nil,"ARTWORK") 
        f.header.text:SetFont("Fonts\\ARIALN.ttf", 9, "OUTLINE")
        f.header.text:SetPoint("CENTER",0,0)
        f.header.text:SetText("QuickBook V0.0.1")
        f.close = CreateFrame("Frame", nil, f)
        f.close:SetHeight(16)
        f.close:SetWidth(16)
        f.close.text = f.close:CreateFontString(nil,"ARTWORK") 
        f.close.text:SetFont("Fonts\\ARIALN.ttf", 9, "OUTLINE")
        f.close.text:SetPoint("CENTER",0,0)
        f.close.text:SetText("X")
        f.close:SetPoint("TOPRIGHT", 0, 0)
        f.close:SetScript("OnMouseDown", function(self, button)
            toggleFrame()
        end)
        local ht = f.header:CreateTexture(nil,"BACKGROUND")
        ht:SetTexture("Interface\\AddOns\\QuickBook\\HeaderBgCenter.tga")
        ht:SetAllPoints(f.header)
        f.header.texture = ht
        numRows = math.ceil((((qb_numTabs) * 64) / (qb_itemSize*qb_columnCount)) +0.9)
        f.tabFrame = CreateFrame("Frame", nil, f)
        f.tabFrame:SetWidth(qb_itemSize*qb_columnCount)
        f.tabFrame:SetHeight(16*numRows)
        f.tabFrame:SetPoint("TOPLEFT", 0, -16)
        local t = f.tabFrame:CreateTexture(nil,"BACKGROUND")
        t:SetTexture("Interface\\AddOns\\QuickBook\\HeaderBgCenter.tga")
        t:SetAllPoints(f.tabFrame)
        f.tabFrame.texture = t
        f.tabFrameL = CreateFrame("Frame", nil, f.tabFrame)
        f.tabFrameR = CreateFrame("Frame", nil, f.tabFrame)
        f.tabFrameL:SetWidth(32)
        f.tabFrameR:SetWidth(32)
        f.tabFrameL:SetHeight(16*numRows)
        f.tabFrameR:SetHeight(16*numRows)
        f.tabFrameL:SetPoint("LEFT", 0, 0)
        f.tabFrameR:SetPoint("RIGHT", 0, 0)
        local t1 = f.tabFrameL:CreateTexture(nil,"BACKGROUND")
        t1:SetTexture("Interface\\AddOns\\QuickBook\\HeaderBgLeft.tga")
        t1:SetAllPoints(f.tabFrameL)
        f.tabFrameL.texture = t1
        local t2 = f.tabFrameR:CreateTexture(nil,"BACKGROUND")
        t2:SetTexture("Interface\\AddOns\\QuickBook\\HeaderBgRight.tga")
        t2:SetAllPoints(f.tabFrameR)
        f.tabFrameR.texture = t2
        f.tabs = {}
   
        for i = 1, qb_numTabs, 1
        do
            local itemNum = i-1
            local itemsInRow = math.floor((qb_itemSize*qb_columnCount)/64)
            local itemInRow =  math.fmod(itemNum, itemsInRow)
            local rowNum = math.floor(itemNum/itemsInRow)
            f.tabs[i] = CreateFrame("Button", nil, f.tabFrame)
            f.tabs[i]:SetFrameStrata("HIGH")
            f.tabs[i]:SetWidth(64) 
            f.tabs[i]:SetHeight(16)
            f.tabs[i]:SetPoint("TOPLEFT",((itemInRow)*64),-(16*(rowNum)))
            f.tabs[i].text = f.tabs[i]:CreateFontString(nil,"ARTWORK") 
            f.tabs[i].text:SetFont("Fonts\\ARIALN.ttf", 9, "OUTLINE")
            f.tabs[i].text:SetPoint("CENTER",0,0)
            f.tabs[i].text:SetText(getGroupDescription(i))
            f.tabs[i]:SetScript("OnMouseDown", function(self, button)
                qb_currentGroup = i
                updateFrame()
            end)
            f.tabs[i]:Show()
        end
    end
    for i = 1, #f.tabs, 1
    do
        if i > qb_numTabs then
            f.tabs[i]:Hide()
        else
            f.tabs[i]:Show()
        end
    end
    local updateTabHeader = false
    for i = 1, qb_numTabs, 1
    do
        if f.tabs[i] == nil then
            updateTabHeader = true
            local itemNum = i-1
            local itemsInRow = math.floor((qb_itemSize*qb_columnCount)/64)
            local itemInRow =  math.fmod(itemNum, itemsInRow)
            local rowNum = math.floor(itemNum/itemsInRow)
            f.tabs[i] = CreateFrame("Button", nil, f.tabFrame)
            f.tabs[i]:SetFrameStrata("HIGH")
            f.tabs[i]:SetWidth(64) 
            f.tabs[i]:SetHeight(16)
            f.tabs[i]:SetPoint("TOPLEFT",((itemInRow)*64),-(16*(rowNum)))
            f.tabs[i].text = f.tabs[i]:CreateFontString(nil,"ARTWORK") 
            f.tabs[i].text:SetFont("Fonts\\ARIALN.ttf", 9, "OUTLINE")
            f.tabs[i].text:SetPoint("CENTER",0,0)
            f.tabs[i].text:SetText(getGroupDescription(i))
            
            f.tabs[i]:SetScript("OnMouseDown", function(self, button)
                qb_currentGroup = i
                updateFrame()
            end)
            f.tabs[i]:Show()
        end
        f.tabFrame:SetWidth(qb_itemSize*qb_columnCount)
        local itemsInRow = math.floor((qb_itemSize*qb_columnCount)/64)
        local numRows = math.floor((qb_numTabs - 1)/itemsInRow) + 1
        f.tabFrame:SetHeight(16*numRows)
        f.tabFrameL:SetHeight(16*numRows)
        f.tabFrameR:SetHeight(16*numRows)
        f.tabFrame:SetPoint("TOPLEFT", 0, -16)
        f.tabs[i].text:SetText(getGroupDescription(i))
        if i == qb_currentGroup then
            f.tabs[i].text:SetFont("Fonts\\ARIALN.ttf", 11, "OUTLINE")
            f.tabs[i].text:SetPoint("CENTER",0,0)
        else
            f.tabs[i].text:SetFont("Fonts\\ARIALN.ttf", 9, "OUTLINE")
            f.tabs[i].text:SetPoint("CENTER",0,0)
        end
    end
end

function updateFrame()
    if qb_groups == nil then
        return
    end
    if f.frames == nil then
        f.frames = {}
        f:SetPoint("CENTER",0,0)
    end
    f:SetWidth(qb_itemSize*qb_columnCount)
    local q = qb_groups
    if qb_global_tabs[qb_currentGroup] then
        q = qb_global_groups
    end
    local rows = q[qb_currentGroup]
    if rows == nil then
        q[qb_currentGroup] = {
            description = "new group",
            rows = {}
        }
        rows = q[qb_currentGroup]
    end
    f:SetHeight(qb_itemSize + (qb_rowCount * qb_itemSize)) 
    updateHeader(f)
    for i = 1, #f.frames, 1
    do
        for y = 1, #f.frames[i], 1
        do
            if i > qb_rowCount or y > qb_columnCount then
                f.frames[i][y]:Hide()
            end
        end
    end
    for i = 1, qb_rowCount, 1
    do
        local row = rows.rows[i]
        if row == nil then
            row = {}
            rows.rows[i] = row
        end
        if f.frames[i] == nil then
            f.frames[i] = {}
        end
        for y = 1, qb_columnCount, 1
        do
            local itemFrame = f.frames[i][y]
            if itemFrame == nil then
                itemFrame = createItemFrame(f, row[y], i, y)
                f.frames[i][y] = itemFrame
            end
            updateItemFrame(itemFrame, row[y], i, y)            
        end
    end
end

function setItem(row, column, item)
    local rows = qb_groups[qb_currentGroup]
    if qb_global_tabs[qb_currentGroup] then
        rows = qb_global_groups[qb_currentGroup]
    end
    rows.rows[row][column] = item
end

function removeItem(row, column)
    local g = qb_groups
    if qb_global_tabs[qb_currentGroup] then 
        g = qb_global_groups
    end
    g_item = g[qb_currentGroup].rows[row][column]
    g[qb_currentGroup].rows[row][column] = nil
    updateFrame()
end

function createItemFrame(parent, item, row, column)
    local f = CreateFrame("Button", nil, parent, "SecureActionButtonTemplate")
    f:SetFrameStrata("HIGH")
    f:SetWidth(qb_itemSize) 
    f:SetHeight(qb_itemSize) 
    f:SetScript("OnUpdate", QuickBook_Item_OnUpdate)
    f.cooldown = CreateFrame("Cooldown", nil, f, "CooldownFrameTemplate")
    f.cooldown:SetWidth(qb_itemSize) 
    f.cooldown:SetHeight(qb_itemSize)
    f.cooldown:SetAllPoints(f)
    f.cooldown:SetSwipeColor(1, 1, 1)
    f.cooldown:Show()
    f.text = f:CreateFontString(nil,"ARTWORK") 
    f.text:SetFont("Fonts\\ARIALN.ttf", qb_fontSize, "OUTLINE")
    f.text:SetPoint("CENTER",0,qb_fontSize/2)
    f.ltext = f:CreateFontString(nil,"ARTWORK") 
    f.ltext:SetFont("Fonts\\ARIALN.ttf", qb_fontSizeSM, "OUTLINE")
    f.ltext:SetPoint("CENTER",0,-(qb_fontSizeSM/2))
    
    
    local t = f:CreateTexture(nil,"HIGH")
    t:SetTexture("Interface\\AddOns\\QuickBook\\EmptyItemBackGround.tga")
    t:SetAllPoints(f)
    f.texture = t

    f:SetScript("OnMouseDown", function(self, button)
        local itemType, id, itemLink = GetCursorInfo()
        if itemType ~= nil then
            f:SetAttribute("macrotext1", "")
            f:SetAttribute("type1", nil)
        end
    end)
    f:SetScript("OnMouseUp", function(self, button)
        local itemType, id, itemLink = GetCursorInfo()
        ClearCursor()
        if itemType == nil and g_item ~= nil then
            setItem(row, column, g_item)
            g_item = nil
            return
        end
        if itemType == "item" then
            local itemIcon = GetItemIcon(id)
            local item = {id=id, link=itemLink}
            setItem(row, column, item)
            --updateFrame()
        elseif itemType == "spell" then
            local spellName, spellSubName = GetSpellBookItemName(id, BOOKTYPE_SPELL)
            local name, rank, icon, castTime, minRange, maxRange, spellId = GetSpellInfo(spellName)
            local link = GetSpellLink(spellName)
            local item = {id=spellId, link=link, spell=name, type="spell"}
            setItem(row, column, item)
        elseif itemType == "mount" then
            local name, spellID, icon, isActive, isUsable, sourceType, isFavorite, isFactionSpecific, faction, shouldHideOnChar, isCollected, mountID
            = C_MountJournal.GetMountInfoByID(id)
            local link = GetSpellLink(spellID)
            local item = {id=id, link=link, icon=icon, spell=name, type="mount"}
            setItem(row, column, item)
        elseif itemType == "battlepet" then
            local speciesID, customName, level, xp, maxXp, displayID, isFavorite, name, icon, petType, creatureID, sourceText, description, isWild, canBattle, tradable, unique, obtainable = C_PetJournal.GetPetInfoByPetID(id)

            local item = {id=id, spell=name, icon=icon, type="battlepet"}
            setItem(row, column, item)
        else
            local link = GetSpellLink(spellName)
            local item = {id=spellId, link=link, spell=name, type="spell"}
        end
 
        if itemType == nil and button == "RightButton" then
            removeItem(row, column)
        end
    end)
    f:HookScript("OnEnter", function()
        if (f.item ~= nil and f.item.type == "battlepet" and f.item.spell)then
            local speciesID, customName, level, xp, maxXp, displayID, isFavorite, name, icon, petType, creatureID, sourceText, description, isWild, canBattle, tradable, unique, obtainable = C_PetJournal.GetPetInfoByPetID(f.item.id)
            GameTooltip:SetOwner(f, "ANCHOR_TOP")
            if customName then
                GameTooltip:AddLine(customName, 1, 1, 1)
            end
            GameTooltip:AddLine(name, 1, 1, 1)
            GameTooltip:Show()
        else
            if (f.item ~= nil and f.item.type == "profession") then
                link = GetSpellLink(f.item.id)
                if link then
                    GameTooltip:SetOwner(f, "ANCHOR_TOP")
                    GameTooltip:SetHyperlink(link)
                    GameTooltip:Show()
                end
            else
                if (f.link) then
                    GameTooltip:SetOwner(f, "ANCHOR_TOP")
                    GameTooltip:SetHyperlink(f.link)
                    GameTooltip:Show()
                end
            end
        end
      end)
       
    f:HookScript("OnLeave", function()
        GameTooltip:Hide()
    end)
    f:Show()
    return f
end


function updateItemFrame(f, item, row, column)
    f:SetWidth(qb_itemSize) 
    f:SetHeight(qb_itemSize) 
    if f.item ~= item then
        if f.texture == nil then
            local t = f:CreateTexture("icon","HIGH")
            f.texture = t
        end
        local t = f.texture
        if item == nil then
            f.link = nil
            f.item = nil
            t:SetTexture("Interface\\AddOns\\QuickBook\\EmptyItemBackGround.tga")
            f:SetAttribute("macrotext1", nil)
            f:SetAttribute("type1", nil)
            f:SetAttribute("spell1", nil)
            t:SetAllPoints(f)
            f.cooldown:Hide()
        else
            f.link = item.link
            f.item = item
            if item.icon then
                itemIcon = item.icon
            else
                if item.type == "spell" then
                    name, rank, itemIcon, castTime, minRange, maxRange, spellId = GetSpellInfo(item.id)
                elseif item.type == "profession" then
                    recipeInfo = C_TradeSkillUI.GetRecipeInfo(item.id)
                    itemIcon = recipeInfo.icon
                elseif item.type == "mount" then
                    if item.id == 0 then
                        f.link = "|cffffd000|Hspell:150544|h[Zufälliges Lieblingsreittier beschwören]|h|r"
                        itemIcon = 413588
                    end
                    --name, rank, itemIcon, castTime, minRange, maxRange, spellId = GetSpellInfo(item.id)
                    --itemIcon = GetItemIcon(item.itemId)
                elseif item.type == "battlepet" then
                    if item.id == "BattlePet-0-FFFFFFFFFFFFFF" then
                        f.link = "|cffffd000|Hspell:243819|h[Zufälliges Lieblingskampfhaustier beschwören]|h|r"
                        itemIcon = 652131
                    end

                else 
                    itemIcon = GetItemIcon(item.id)
                end
            end
            t:SetTexture(itemIcon)
            t:SetAllPoints()
            f.texture = t
            if item.type == "spell" then
                f:SetAttribute("type1", "spell")
                f:SetAttribute("spell1", item.spell)
            elseif item.type == "profession" then
                f:SetAttribute("type1", "macro")
                f:SetAttribute("macrotext1", "/run C_TradeSkillUI.OpenTradeSkill("..item.profession..")\n".."/run C_TradeSkillUI.CraftRecipe("..item.id..")\n".."/run C_TradeSkillUI.CloseTradeSkill()")
            elseif item.type == "mount" then
                f:SetAttribute("type1", "macro")
                f:SetAttribute("macrotext1", "/run C_MountJournal.SummonByID(\""..item.id.."\")")
            elseif item.type == "battlepet" then
                f:SetAttribute("type1", "macro")
                f:SetAttribute("macrotext1", "/run C_PetJournal.SummonPetByGUID(\""..item.id.."\")")
                -- /run C_PetJournal.SummonPetByGUID("BattlePet-0-000004A04DD0")
            else
                f:SetAttribute("type1", "macro")
                f:SetAttribute("macrotext1", "/use item:"..item.id)
            end
        end
    end
    if item ~= nil and item.type == nil then
        local countTotal = GetItemCount(item.id, true)
        local countLocal = GetItemCount(item.id, false)
        f.text:SetText(countTotal)
        f.ltext:SetText("(" .. countLocal .. ")")
    else
        f.text:SetText("")
        f.ltext:SetText("")
    end
    local headerHeight = 32
    if f:GetParent().tabFrame ~= nil then
        headerHeight = f:GetParent().tabFrame:GetHeight()
    else
        print("tab frame is nil")
    end
    f:SetPoint("TOPLEFT",(column-1)*qb_itemSize,(-(row-1)*qb_itemSize)-16-headerHeight)
    f:Show()
    return f
end

function toggleFrame()
    if f == nil then
        createFrame()
        f:Show()
        return
    end
    if f:IsVisible() then
        f:Hide()
    else
        updateFrame()
        f:Show()
    end
end

-- Globals Section
QuickBook_UpdateInterval = 0.4; -- How often the OnUpdate code will run (in seconds)
f.TimeSinceLastUpdate = 0;
-- Functions Section
function QuickBook_OnUpdate(self, elapsed)
  self.TimeSinceLastUpdate = self.TimeSinceLastUpdate + elapsed; 	

  if (self.TimeSinceLastUpdate > QuickBook_UpdateInterval) then
    --
    -- Insert your OnUpdate code here
    --
    updateFrame()
    self.TimeSinceLastUpdate = 0;
  end
end

f:SetScript("OnUpdate", QuickBook_OnUpdate)

function QuickBook_Item_OnUpdate(self, elapsed)
    if self.TimeSinceLastUpdate == nil then
        self.TimeSinceLastUpdate = 0
    else
        self.TimeSinceLastUpdate = self.TimeSinceLastUpdate + elapsed; 	
    end
  
    if (self.TimeSinceLastUpdate > QuickBook_UpdateInterval) then
      --
      -- Insert your OnUpdate code here
      --
      if self.item ~= nil then
          if self.item.type == "spell" then
              startTime, duration, enable = GetSpellCooldown(self.item.spell)
          elseif self.item.type == "profession" then
              startTime, duration, enable = GetSpellCooldown(self.item.id)
          elseif self.item.type == "battlepet" then
              duration = 0
          else
              startTime, duration, enable = GetItemCooldown(self.item.id)
          end
          if f.duration == 0 then
              --61304 
              startTime, duration, enable = GetSpellCooldown(61304)
          end
          if duration > 0 then
            self.cooldown:SetCooldown(startTime, duration)
            self.cooldown:Show()
          else
            self.cooldown:Hide()
          end
      end
      self.TimeSinceLastUpdate = 0;
    end
  end

function createMinimapButton()
        -- Create minimap button
    
    local minibtn = CreateFrame("Button", nil, Minimap)
    minibtn:SetFrameLevel(8)
    minibtn:SetSize(32,32)
    minibtn:SetMovable(true)
    
    minibtn:SetNormalTexture("Interface\\AddOns\\QuickBook\\QuickActionsIcon.tga")
    minibtn:SetPushedTexture("Interface\\AddOns\\QuickBook\\QuickActionsIcon.tga")
    minibtn:SetHighlightTexture("Interface\\AddOns\\QuickBook\\QuickActionsIcon.tga")
    
    
    local myIconPos = 0
    minibtn:RegisterForDrag("LeftButton")
    minibtn:SetScript("OnDragStart", function()
        minibtn:StartMoving()
        minibtn:SetScript("OnUpdate", UpdateMapBtn)
    end)
    
    minibtn:SetScript("OnDragStop", function()
        minibtn:StopMovingOrSizing();
        minibtn:SetScript("OnUpdate", nil)
        --UpdateMapBtn();
    end)
    
    -- Set position
    minibtn:ClearAllPoints();
    minibtn:SetPoint("TOPLEFT", Minimap, "TOPLEFT", 52 - (80 * cos(myIconPos)),(80 * sin(myIconPos)) - 52)
    
    -- Control clicks
    minibtn:SetScript("OnClick", function()
        toggleFrame()
    end)
end
 
-- Control movement
local function UpdateMapBtn()
    local Xpoa, Ypoa = GetCursorPosition()
    local Xmin, Ymin = Minimap:GetLeft(), Minimap:GetBottom()
    Xpoa = Xmin - Xpoa / Minimap:GetEffectiveScale() + 70
    Ypoa = Ypoa / Minimap:GetEffectiveScale() - Ymin - 70
    myIconPos = math.deg(math.atan2(Ypoa, Xpoa))
    minibtn:ClearAllPoints()
    minibtn:SetPoint("TOPLEFT", Minimap, "TOPLEFT", 52 - (80 * cos(myIconPos)), (80 * sin(myIconPos)) - 52)
end

local function split (inputstr, sep)
    if sep == nil then
            sep = "%s"
    end
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
            table.insert(t, str)
    end
    return t
end


createMinimapButton()


local function QuickBookCommands(msg, editbox)
    local _, _, cmd, args = string.find(msg, "%s?(%w+)%s?(.*)")
    if cmd == "setItemSize" and args ~= "" then
        qb_itemSize = tonumber(args)
    elseif cmd == "setColumnCount" and args ~= "" then
        qb_columnCount = tonumber(args)
    elseif cmd == "setRowCount" and args ~= "" then
        qb_rowCount = tonumber(args)
    elseif cmd == "setGroup" and args ~= "" then
        qb_currentGroup = tonumber(args)
    elseif cmd == "addRecipe" and args ~= "" then
        local res = split(args, ",")
        local row = res[1]
        local column = res[2]
        local profession = res[3]
        local spell = res[4]
        local name, rank, icon, castTime, minRange, maxRange, spellId = GetSpellInfo(tonumber(spell))
        --local sName, link, iRarity, iLevel, iMinLevel, sType, sSubType, iStackCount = GetItemInfo(tonumber(item))
        setItem(tonumber(row), tonumber(column), {id=spellId, spell=name, type="profession", profession=profession})
    elseif cmd == "setGroupDescription" then
        local res = split(args)
        setGroupDescription(tonumber(res[1]), res[2])
    elseif cmd == "setNumTabs" then
        qb_numTabs = tonumber(args)
    elseif cmd == "show" then
        f:Show()
    elseif cmd == "hide" then
        f:Hide()
    elseif cmd == "toggle" then
        toggleFrame()
    elseif cmd == "setGlobalTab" then
        qb_global_tabs[tonumber(args)] = true
        qb_global_groups[tonumber(args)] = qb_groups[tonumber(args)]
    else
      -- /ql addRecipe 2,1,Alchemie,301683,171268
      -- /ql addRecipe 2,2,Alchemie,307142,171428
      -- /ql addRecipe 3,1,Kochkunst,308410,172046
    end
  end
  
  SLASH_QUICKBOOK1, SLASH_QUICKBOOK2 = '/ql', '/quicklook'
  
  SlashCmdList["QUICKBOOK"] = QuickBookCommands