## Interface: 90002
## Title: QuickBook
## Author: Pya - Durotan
## Version: 0.1
## SavedVariables: qb_global_tabs, qb_global_groups
## SavedVariablesPerCharacter: qb_itemSize, qb_rowCount, qb_columnCount, qb_fontSize, qb_fontSizeSM, qb_currentGroup, qb_groups, qb_numTabs
main.lua